function Controller() {
    function init(videoId) {
        video.getData(videoId, function(videoData) {
            if (videoData) {
                var openButton = Ti.UI.createButton({
                    title: "Start Video",
                    top: "75",
                    height: "40dp",
                    left: "10dp",
                    right: "10dp"
                });
                $.win.add(openButton);
                openButton.addEventListener("click", function() {
                    var activeMovie = Titanium.Media.createVideoPlayer({
                        backgroundColor: "blue",
                        mediaControlStyle: Titanium.Media.VIDEO_CONTROL_DEFAULT,
                        scalingMode: Titanium.Media.VIDEO_SCALING_ASPECT_FILL,
                        fullscreen: true,
                        autoplay: true,
                        url: "http://player.vimeo.com/video/84343992"
                    });
                    var closeButton = Ti.UI.createButton({
                        title: "Exit Video",
                        top: "50",
                        height: "40dp",
                        left: "10dp",
                        right: "10dp"
                    });
                    closeButton.addEventListener("click", function() {
                        activeMovie.hide();
                        activeMovie.release();
                        activeMovie = null;
                    });
                    activeMovie.add(closeButton);
                });
                var label = Ti.UI.createLabel({
                    text: "Its OK, you're doing great!",
                    top: "0"
                });
                $.win.add(label);
            }
        });
        var label2 = Ti.UI.createLabel({
            text: "Loading...",
            top: "20"
        });
        $.win.add(label2);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.win = Ti.UI.createWindow({
        backgroundColor: "white",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var video = require("video");
    $.win.open();
    init("84343992");
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;