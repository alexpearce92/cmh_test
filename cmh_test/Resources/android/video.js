function getData(videoId, onSuccess) {
    var url = "http://vimeo.com/api/v2/video/" + videoId + ".json";
    var client = Ti.Network.createHTTPClient({
        onload: function() {
            json = JSON.parse(this.responseText);
            Ti.API.debug(json);
            onSuccess(json);
        },
        onerror: function(e) {
            Ti.API.debug(e.error);
            alert("Could not retrieve current data!");
        }
    });
    client.open("GET", url);
    client.send();
}

var videoData = {
    getData: function(videoId, onSuccess) {
        return getData(videoId, onSuccess);
    }
};

module.exports = videoData;